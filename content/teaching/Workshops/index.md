---
date: "2020-07-10"
external_link: ""
image:
  caption: 
  focal_point: 
links:
slides: 
summary: Invited workshops taught
tags:
- teaching
title: Workshops
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

I have delivered numerous workshops on eye-tracking methods/design (e.g., Visual World Paradigm, Experiment Builder and Data Viewer) and data analysis (e.g., Linear mixed-effects regression, Generalized additive mixed-effects regression) at a variety of institutions across Canada and Europe.

From 2015 to 2019, I also taught various courses for STEP (Spring Training in Experimental Psycholinguistics - <a href="http://ccp.artsrn.ualberta.ca/stepccp/" target="_blank">STEP@CCP</a>) which offers hands-on courses in experimental psycholinguistics at the University of Alberta. 

For more information about the courses/workshops I have taught, please refer to my [CV](https://vincentporretta.gitlab.io/files/CV_Porretta.pdf).

