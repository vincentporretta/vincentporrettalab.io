---
date: "2020-07-10"
external_link: 
image:
  caption: 
  focal_point: 
summary: University-level courses taught
tags:
- teaching
title: University Teaching
---

I have designed and taught various university-level courses at a number of institutions.

* University of Alberta: *Introduction to Linguistic Analysis*, *Introduction to Engilsh Syntax*, and *Introduction to Psycholinguistic Data Analysis*

* McMaster University: *Computers and Linguistic Analysis*, *Programming for Linguists*, and *Phonology*

* Brock University: *Introduction to General Linguistics*