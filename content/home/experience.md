+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Language engineer"
  company = "Amazon"
  company_url = ""
  location = "Boston, MA"
  date_start = "2020-09-14"
  date_end = ""
  description = """"""

[[experience]]
  title = "Sessional faculty"
  company = "McMaster University"
  company_url = ""
  location = "Ontario"
  date_start = "2019-09-01"
  date_end = "2020-04-30"
  description = """"""

[[experience]]
  title = "Sessional faculty"
  company = "Brock University"
  company_url = ""
  location = "Ontario"
  date_start = "2019-09-01"
  date_end = "2019-12-31"
  description = """"""

[[experience]]
  title = "Postdoctoral research fellow"
  company = "University of Windsor"
  company_url = ""
  location = "Ontario"
  date_start = "2017-06-01"
  date_end = "2019-05-31"
  description = """"""

[[experience]]
  title = "Postdoctoral research fellow"
  company = "University of Alberta"
  company_url = ""
  location = "Alberta"
  date_start = "2015-09-01"
  date_end = "2017-05-31"
  description = """"""

[[experience]]
  title = "Foreign Language Teacher"
  company = "Bishop Hartley HS"
  company_url = ""
  location = "Ohio"
  date_start = "2007-09-01"
  date_end = "2010-08-31"
  description = """"""
  
+++
