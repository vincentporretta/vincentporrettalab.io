+++
# A Skills section created with the Featurette widget.
widget = "featurette"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 20  # Order that this section will appear.

title = "Skills"
subtitle = ""

# Showcase personal skills or business features.
# 
# Add/remove as many `[[feature]]` blocks below as you like.
# 
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons

[[feature]]
  icon = "fas fa-comments"
  icon_pack = "fas"
  name = "Linguistic Research"
  description = "Language processing, Acoustic analysis"
  
[[feature]]
  icon = "fas fa-school"
  icon_pack = "fas"
  name = "Teaching and Learning"
  description = "Pedagogy, Curriculum Development"
  
[[feature]]
  icon = "r-project"
  icon_pack = "fab"
  name = "R"
  description = "R, Shiny, R Markdown"
  
[[feature]]
  icon = "chart-line"
  icon_pack = "fas"
  name = "Statistics"
  description = "LMER, GAMM, Time-series Analysis"  
  
[[feature]]
  icon = "fas fa-language"
  icon_pack = "fas"
  name = "Languages"
  description = "English, Italian, Spanish"


# Uncomment to use emoji icons.
# [[feature]]
#  icon = ":smile:"
#  icon_pack = "emoji"
#  name = "Emojiness"
#  description = "100%"  

# Uncomment to use custom SVG icons.
# Place custom SVG icon in `assets/images/icon-pack/`, creating folders if necessary.
# Reference the SVG icon name (without `.svg` extension) in the `icon` field.
# [[feature]]
#  icon = "your-custom-icon-name"
#  icon_pack = "custom"
#  name = "Surfing"
#  description = "90%"

+++
