---
abstract: ""
authors: 
- Vincent Porretta
- Benjamin V. Tucker
date: 2017-09-07T13:00:00
date_end: 2017-09-07T13:00:00
draft: false
event: "Architectures and Mechanisms for Language Processing (AMLaP) 23"
event_url: ""
featured: false
location: "Mo, UK"
projects:  ""
publishDate: 2017-09-07
selected: false
summary: ""
tags: 
- accented speech
- pupil dilation
- eye-tracking
- gamm
- listener experience
title: What big eyes you have - Pupillary response to intelligibility of foreign-accented speech
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
