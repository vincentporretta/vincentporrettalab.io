---
abstract: ""
authors: 
- Vincent Porretta
- Aki-Juhani Kyröläinen
- Benjamin V. Tucker
date: 2013-06-23T13:00:00
date_end: 2013-06-23T13:00:00
draft: false
event: "International Cognitive Linguistics Conference (ICLC) 12"
event_url: ""
featured: false
location: "Edmonton, AB, Canada"
projects:  ""
publishDate: 2013-06-23
selected: false
summary: ""
tags: 
- accented speech
- gamm
title: Influences on perceived foreign accentedness - Acoustic distances and lexical neighborhoods
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
