---
abstract: ""
authors: 
- Vincent Porretta
- Aki-Juhani Kyröläinen
- Jacolien van Rij
- Juhani Järvikivi
date: 2015-09-28T13:00:00
date_end: 2015-09-28T13:00:00
draft: false
event: "New Developments in the Quantitative Study of Languages, Symposium of the Linguistics Association of Finland (SKY)"
event_url: ""
featured: false
location: "Helsinki, Finland"
projects:  ""
publishDate: 2015-09-28
selected: false
summary: ""
tags: 
- vwp
- eye-tracking
- gamm
title: A step forward in the analysis of visual world eye-tracking data
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
