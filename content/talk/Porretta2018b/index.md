---
abstract: ""
authors: 
- Vincent Porretta
- Juhani Järvikivi
- Lori Buchanan
date: 2018-06-20T13:00:00
date_end: 2018-06-20T13:00:00
draft: false
event: "The 1st international Workshop on Predictive Processing"
event_url: ""
featured: false
location: "San Sebastián, Spain"
projects:  ""
publishDate: 2018-06-20
selected: false
summary: ""
tags: 
- accented speech
- vwp
- eye-tracking
- prediction
- gamm
title: Predicting from the less certain - Predictive processing and foreign-accented speech
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
