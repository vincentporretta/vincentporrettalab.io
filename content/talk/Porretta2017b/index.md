---
abstract: ""
authors: 
- Vincent Porretta
- Aki-Juhani Kyröläinen
- Jacolien van Rij
- Juhani Järvikivi
date: 2017-06-21T13:00:00
date_end: 2017-06-21T13:00:00
draft: false
event: "The 9th KES International Conference on Intelligent Decision Technologies (KES-IDT 2017) -- Eye Movement Data Processing and Analysis"
event_url: ""
featured: false
location: "Vilamoura, Portugal"
projects:  ""
publishDate: 2017-06-21
selected: false
summary: ""
tags: 
- vwp
- eye-tracking
- gamm
title: Visual world paradigm data - From preprocessing to nonlinear time-course analysis
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
