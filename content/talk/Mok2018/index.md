---
abstract: ""
authors: 
- Ivy Mok
- Anja Arnhold
- Vincent Porretta
- Saskia Verstegen
- Maeghan Jerry
- Aoju Chen
- Juhani Järvikivi
date: 2018-06-19T13:00:00
date_end: 2018-06-19T13:00:00
draft: false
event: "The 16th Conference on Laboratory Phonology"
event_url: ""
featured: false
location: "Lisbon, Portugal"
projects:  ""
publishDate: 2018-06-19
selected: false
summary: ""
tags: 
- accent
- information status
- vwp
- eye-tracking
- prediction
- gamm
title: Processing of intonation and information status in British English by Chinese L2 and Canadian L1 speakers
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
