---
abstract: ""
authors: 
- Vincent Porretta
- Benjamin V. Tucker
date: 2015-09-10T13:00:00
date_end: 2015-09-10T13:00:00
draft: false
event: "The 18th International Congress of Phonetic Sciences"
event_url: ""
featured: false
location: "Glasgow, UK"
projects:  ""
publishDate: 2015-09-10
selected: false
summary: ""
tags: 
- accented speech
- gamm
title: Intelligibility of foreign-accented words - Acoustic distances and gradient foreign accentedness
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
