---
abstract: ""
authors: 
- Vincent Porretta
- Juhani Järvikivi
- Benjamin V. Tucker
date: 2014-09-03T13:00:00
date_end: 2014-09-03T13:00:00
draft: false
event: "Architectures and Mechanisms for Language Processing (AMLaP) 20"
event_url: ""
featured: false
location: "Edinburgh, UK"
projects:  ""
publishDate: 2014-09-03
selected: false
summary: ""
tags: 
- accented speech
- reaction times
- vwp
- eye-tracking
- gamm
- listener experience
title: The effect of gradient foreign accentedness and listener experience on lexical activation strength and access
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
