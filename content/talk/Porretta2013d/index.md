---
abstract: ""
authors: 
- Vincent Porretta
date: 2013-11-15T13:00:00
date_end: 2013-11-15T13:00:00
draft: false
event: "Colloquium Series"
event_url: ""
featured: false
location: "University of Utah, Department of Linguistics"
projects:  ""
publishDate: 2013-11-15
selected: false
summary: ""
tags: 
- invited
- accented speech
- ratings
- erp
title: Gradient foreign accent - Ratings and event-related potentials
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
