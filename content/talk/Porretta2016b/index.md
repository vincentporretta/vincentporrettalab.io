---
abstract: ""
authors: 
- Vincent Porretta
- Yoichi Mukai
- Benjamin V. Tucker
date: 2016-10-19T13:00:00
date_end: 2016-10-19T13:00:00
draft: false
event: "The 10th International Conference on the Mental Lexicon"
event_url: ""
featured: false
location: "Ottawa, ON, Canada"
projects:  ""
publishDate: 2016-10-19
selected: false
summary: ""
tags: 
- pupil dilation
- gamm
- eye-tracking
title: What big eyes you have - Pupillary response to reduced speech
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
