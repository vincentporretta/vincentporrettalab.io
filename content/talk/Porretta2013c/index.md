---
abstract: ""
authors: 
- Vincent Porretta
- Antoine Tremblay
- Patrick Bolger
date: 2013-07-14T13:00:00
date_end: 2013-07-14T13:00:00
draft: false
event: "Linguistic Society of America Workshop: How the brain accommodates variability in linguistic representations"
event_url: ""
featured: false
location: "Ann Arbor, MI, USA"
projects:  ""
publishDate: 2013-07-14
selected: false
summary: ""
tags: 
- accented speech
- erp
- gamm
- listener experience
title: Auditory prelexical processing of foreign-accented speech
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
