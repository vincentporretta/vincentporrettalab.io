---
abstract: ""
authors: 
- Vincent Porretta
date: 2017-03-03T13:00:00
date_end: 2017-03-03T13:00:00
draft: false
event: "Colloquium Series"
event_url: ""
featured: false
location: "University of Alberta, Department of Linguistics"
projects:  ""
publishDate: 2017-03-03
selected: false
summary: ""
tags: 
- invited
- accented speech
- rating
- erp
- vwp
- eye-tracking
- pupil dilation
- gamm
- listener experience
title: Processing of foreign-accented speech - From the perfectly ordinary to the incomprehensible---and everything in between
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
