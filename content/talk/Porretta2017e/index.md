---
abstract: ""
authors: 
- Vincent Porretta
- Aki-Juhani Kyröläinen
date: 2017-09-07T13:00:00
date_end: 2017-09-07T13:00:00
draft: false
event: "Architectures and Mechanisms for Language Processing (AMLaP) 23"
event_url: ""
featured: false
location: "Lancaster, UK"
projects:  ""
publishDate: 2017-09-07
selected: false
summary: ""
tags: 
- accented speech
- vwp
- eye-tracking
- gamm
- lexical competition
title: Expanding competition space - The influence of foreign accentedness on lexical competition
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
