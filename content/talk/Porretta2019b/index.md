---
abstract: ""
authors: 
- Vincent Porretta
- Aki-Juhani Kyröläinen
- Ghadir Nassereddine
- Lori Buchanan
date: 2019-11-14T13:00:00
date_end: 2019-11-14T13:00:00
draft: false
event: "Psychonomics"
event_url: ""
featured: false
location: "Montreal, QC, Canada"
projects:  ""
publishDate: 2019-11-14
selected: false
summary: ""
tags: 
- accented speech
- memory
- semantics
- phonology
title: When 'bug' creeps into memory - False memories and foreign-accented speech
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
