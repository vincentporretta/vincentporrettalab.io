---
abstract: ""
authors: 
- Cat Davies
- Vincent Porretta
- Kremena Koleva
- Ekaterini Klepousniotou
date: 2017-09-07T13:00:00
date_end: 2017-09-07T13:00:00
draft: false
event: "Architectures and Mechanisms for Language Processing (AMLaP) 23"
event_url: ""
featured: false
location: "Lancaster, UK"
projects:  ""
publishDate: 2017-09-07
selected: false
summary: ""
tags: 
- vwp
- eye-tracking
- semantics
- language processing
title: Do speaker-specific cues influence ambiguous word interpretation?
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
