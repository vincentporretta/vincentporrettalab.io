---
abstract: ""
authors: 
- Vincent Porretta
date: 2011-10-22T13:00:00
date_end: 2011-10-22T13:00:00
draft: false
event: "Alberta Conference on Linguistics"
event_url: ""
featured: false
location: "Edmonton, AB, Canada"
projects:  ""
publishDate: 2011-10-22
selected: false
summary: ""
tags:  ""
title: English comparatives and superlatives - Emergence of the double-marked
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
