---
abstract: ""
authors: 
- Vincent Porretta
- Benjamin V. Tucker
date: 2012-07-06T13:00:00
date_end: 2012-07-06T13:00:00
draft: false
event: "Second Language Acquisition of Phonology"
event_url: ""
featured: false
location: "York, UK"
projects:  ""
publishDate: 2012-07-06
selected: false
summary: ""
tags: 
- second language learning
- perception
title: Perception of non-native contrast - Consonant length and L1 English listeners
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
