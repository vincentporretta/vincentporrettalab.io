---
abstract: ""
authors: 
- Juhani Järvikivi
- Vincent Porretta
- Johanne Paradis
- Krithika Govindarajan
- Kayla Day
date: 2017-07-17T13:00:00
date_end: 2017-07-17T13:00:00
draft: false
event: "The 14th International Congress for the Study of Child Language"
event_url: ""
featured: false
location: "Lyon, France"
projects:  ""
publishDate: 2017-07-17
selected: false
summary: ""
tags: 
- vwp
- eye-tracking
- gamm
- language development
- language processing
title: Language knowledge predicts 3--6 year-old mono- and bilingual children’s prounoun processing
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
