---
abstract: ""
authors: 
- Benjamin V. Tucker
- Vincent Porretta
- Yoichi Mukai
date: 2019-05-13T13:00:00
date_end: 2019-05-13T13:00:00
draft: false
event: "177th Meeting of the Acoustical Society of America"
event_url: ""
featured: false
location: "Louisville, KY, USA"
projects:  ""
publishDate: 2019-05-13
selected: false
summary: ""
tags: 
- pupil dilation
- eye-tracking
- gamm
- lexical processing
title: Investigating the comprehension and perception of reduced speech with pupillary response
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
