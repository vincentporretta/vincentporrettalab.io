---
abstract: ""
authors: 
- Saskia Verstegen
- Vincent Porretta
- Aoju Chen
- Juhani Järvikivi
date: 2016-09-01T13:00:00
date_end: 2016-09-01T13:00:00
draft: false
event: "Tone and Intonation in Europe"
event_url: ""
featured: false
location: "Canterbury, UK"
projects:  ""
publishDate: 2016-09-01
selected: false
summary: ""
tags: 
- rating
- accent
- information status
title: Assessment of intonation status in British and Canadian English by Canadian listeners
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
