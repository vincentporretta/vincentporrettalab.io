---
abstract: ""
authors: 
- Vincent Porretta
- Lori Buchanan
- Juhani Järvikivi
date: 2018-09-25T13:00:00
date_end: 2018-09-25T13:00:00
draft: false
event: "The 11th International Conference on the Mental Lexicon"
event_url: ""
featured: false
location: "Edmonton, AB. Canada"
projects:  ""
publishDate: 2018-09-25
selected: false
summary: ""
tags: 
- accented speech
- vwp
- eye-tracking
- prediction
- gamm
title: Processing trade-off? Predicting from foreign-accented speech
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
