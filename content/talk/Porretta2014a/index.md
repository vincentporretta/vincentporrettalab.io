---
abstract: ""
authors: 
- Vincent Porretta
- Benjamin V. Tucker
- Juhani Järvikivi
date: 2014-09-30T13:00:00
date_end: 2014-09-30T13:00:00
draft: false
event: "The 9th International Conference on the Mental Lexicon"
event_url: ""
featured: false
location: "Niagara-on-the-Lake, ON, Canada"
projects:  ""
publishDate: 2014-09-30
selected: false
summary: ""
tags: 
- accented speech
- reaction times
- vwp
- eye-tracking
- gamm
- listener experience
title: The influence of gradient foreign accentedness on lexical activation and retrieval
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
