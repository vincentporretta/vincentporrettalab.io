---
abstract: ""
authors: 
- Vincent Porretta
- Benjamin V. Tucker
date: 2013-06-02T13:00:00
date_end: 2013-06-02T13:00:00
draft: false
event: "International Congress on Acoustics"
event_url: ""
featured: false
location: "Montreal, QC, Canada"
projects:  ""
publishDate: 2013-06-02
selected: false
summary: ""
tags: 
- second language learning
- perception
title: Perception of non-native consonant length in naïve English listeners
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
