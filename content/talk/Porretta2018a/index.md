---
abstract: ""
authors: 
- Vincent Porretta
date: 2018-03-02T13:00:00
date_end: 2018-03-02T13:00:00
draft: false
event: "PsychoShorts 2018"
event_url: ""
featured: false
location: "University of Ottawa, Department of Linguistics"
projects:  ""
publishDate: 2018-03-02
selected: false
summary: ""
tags: 
- invited
- accented speech
- rating
- erp
- vwp
- eye-tracking
- pupil dilation
- gamm
- listener experience
title: Variability in spoken language - Foreign-accented speech and its influence on the dynamics of processing
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
