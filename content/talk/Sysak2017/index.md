---
abstract: ""
authors: 
- Hannah Sysak
- Juhani Järvikivi
- Vincent Porretta
date: 2017-10-28T13:00:00
date_end: 2017-10-28T13:00:00
draft: false
event: "Alberta Conference of Linguistics"
event_url: ""
featured: false
location: "Edmonton, AB, Canada"
projects:  ""
publishDate: 2017-10-28
selected: false
summary: ""
tags: 
- vwp
- eye-tracking
- disfluencies
- language processing
title: Effects of disfluencies on speech processing
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
