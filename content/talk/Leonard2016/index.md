---
abstract: ""
authors: 
- Catherine M. Leonard
- Juhani Järvikivi
- Vincent Porretta
- Marilyn Langevin
date: 2016-05-31T13:00:00
date_end: 2016-05-31T13:00:00
draft: false
event: "Speech Prosody 8"
event_url: ""
featured: false
location: "Boston, MA, USA"
projects:  ""
publishDate: 2016-05-31
selected: false
summary: ""
tags: 
- vwp
- eye-tracking
title: Processing of stuttered speech by fluent listeners
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
