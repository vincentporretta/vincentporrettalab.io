---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Vincent Porretta
- Juhani Järvikivi
date: 2015-06-18T13:00:00
date_end: 2015-06-18T13:00:00
draft: false
event: "The 9th International Morphological Processing Conference"
event_url: ""
featured: false
location: "Potsdam, Germany"
projects:  ""
publishDate: 2015-06-18
selected: false
summary: ""
tags: 
- pupil dilation
title: Pupillometry as a window to real time processing of grammatical aspect in Russian
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
