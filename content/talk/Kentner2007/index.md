---
abstract: ""
authors: 
- Melissa Kentner
- C. Lee Link
- Júlian Vásquez
- Vincent Porretta
date: 2007-03-22T13:00:00
date_end: 2007-03-22T13:00:00
draft: false
event: "Ohio Foreign Language Association (OFLA) Conference"
event_url: ""
featured: false
location: "Cleveland, OH, USA"
projects:  ""
publishDate: 2007-03-22
selected: false
summary: ""
tags:  ""
title: Renew, Re-Energize, Refresh=Re-Educate Workshop
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
