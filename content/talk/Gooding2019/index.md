---
abstract: ""
authors: 
- Taylor Gooding
- Vincent Porretta
- Lori Buchanan
date: 2019-06-07T13:00:00
date_end: 2019-06-07T13:00:00
draft: false
event: "29th Annual meeting of the Canadian Society for Brain, Behaviour and Cognitive Science"
event_url: ""
featured: false
location: "Waterloo, ON, Canada"
projects:  ""
publishDate: 2019-06-07
selected: false
summary: ""
tags: 
- eye-tracking
- semantics
title: Punny and funny - Semantic association in pun reading
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
