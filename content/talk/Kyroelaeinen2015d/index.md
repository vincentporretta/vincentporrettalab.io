---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Vincent Porretta
- Juhani Järvikivi
date: 2015-08-16T13:00:00
date_end: 2015-08-16T13:00:00
draft: false
event: "XVIII European Conference on Eye Movements"
event_url: ""
featured: false
location: "Vienna, Austria"
projects:  ""
publishDate: 2015-08-16
selected: false
summary: ""
tags: 
- pupil dilation
- fixation durations
- reaction times
title: The role of morpho-semantic information in processing Russian verbal aspect - Evidence from pupil dilation, fixation durations and reaction times
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
