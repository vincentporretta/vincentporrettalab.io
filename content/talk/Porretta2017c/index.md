---
abstract: ""
authors: 
- Vincent Porretta
date: 2017-04-07T13:00:00
date_end: 2017-04-07T13:00:00
draft: false
event: "Statistics Workshop"
event_url: ""
featured: false
location: "University of Konstanz, Department of Linguistics"
projects:  ""
publishDate: 2017-04-07
selected: false
summary: ""
tags: 
- invited
- vwp
- eye-tracking
- gamm
title: Nonlinear time-course analysis of visual world paradigm eye-tracking data using GAMM
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
