---
abstract: ""
authors: 
- Vincent Porretta
- Benjamin V. Tucker
date: 2012-10-10T13:00:00
date_end: 2012-10-10T13:00:00
draft: false
event: "Annual Conference of the Canadian Acoustical Association"
event_url: ""
featured: false
location: "Banff, AB, Canada"
projects:  ""
publishDate: 2012-10-10
selected: false
summary: ""
tags: 
- accented speech
- perception
title: Predicting accentedness - Acoustic measurements of Chinese-accented English
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
