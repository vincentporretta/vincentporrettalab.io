---
date: "2020-07-10"
external_link: 
image:
  caption: 
  focal_point: 
summary: "VWPre: Tools for Preprocessing Visual World Data"
tags:
- R
- eye-tracking
- VWP
- shiny
- R Markdown
title: VWPre
---

I develop and maintain *VWPre*, an R package for preprocessing and plotting Visual World Paradigm eye-tracking data. This work is in collaboration with <a href="https://akkyro.gitlab.io/" target="_blank">Aki-Juhani Kyröläinen</a>, <a href="http://www.jacolienvanrij.com/" target="_blank">Jacolien van Rij</a> and <a href="https://sites.ualberta.ca/~jarvikiv/" target="_blank">Juhani Järvikivi</a>. The current version of the package can be found on CRAN: 
<a href="https://CRAN.R-project.org/package=VWPre" target="_blank">VWPre</a> 

There are several elaborated vignettes designed to help users become comfortable with the functionality of the package. 

<img src="http://cranlogs.r-pkg.org/badges/VWPre" align="bottom" height="25" width="150"> 

<br><br>
