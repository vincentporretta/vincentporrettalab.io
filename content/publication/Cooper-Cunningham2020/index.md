---
abstract: "We examined the effects of semantic and visual cues to animacy on children’s and adults’ interpretation of ambiguous pronouns, using the visual world paradigm. Participants listened to sentences with object relative clauses that varied the animacy of potential referents, followed by test sentences beginning with a referentially ambiguous subject pronoun 'he'. Participants viewed images of the referents, with semantically inanimate objects (e.g., a TV) shown with or without added facial features. Results from offline verbal report and online gaze data revealed consistent effects of both semantic animacy and visual context on pronoun resolution in both groups: There was a preference for semantically animate referents as antecedent, but this preference decreased or disappeared when semantically inanimate referents had facial features. The results indicate that the use of animacy as a linguistic cue is flexible and responsive to the visual context. They further suggest that like adults (Nieuwland and Van Berkum, 2006), 4-year-olds can already use fictional, here visual, context to adjust their online and offline language comprehension preferences."
authors: 
- Rebecca Cooper-Cunningham
- Monique Charest
- Vincent Porretta
- Juhani Järvikivi
date: "2020-01-01"
doi: "https://dx.doi.org/10.3389/fcomm.2020.576236"
featured: false
projects: ""
publication: Frontiers in Communication
publication_types: 
- "2"
selected: false
summary: ""
title: When couches have eyes - The effect of visual context on children's reference processing
tags: 
- vwp
- eye-tracking
- gamm
- pronoun resolution
- animacy
- visually-situated language comprehension
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
