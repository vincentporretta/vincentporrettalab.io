---
abstract: "The Visual World Paradigm (VWP) is used to study online spoken language processing and produces time-series data. The data present challenges for analysis and they require significant preprocessing and are by nature nonlinear. Here, we discuss VWPre, a new tool for data preprocessing, and generalized additive mixed modeling (GAMM), a relatively new approach for nonlinear time-series analysis (using mgcv and itsadug), which are all available in R. An example application of GAMM using preprocessed data is provided to illustrate its advantages in addressing the issues inherent to other methods, allowing researchers to more fully understand and interpret VWP data."
authors: 
- Vincent Porretta
- Aki-Juhani Kyröläinen
- Jacolien van Rij
- Juhani Järvikivi
date: "2018-01-01"
doi: "https://dx.doi.org/10.1007/978-3-319-59424-8_25"
featured: false
projects: ""
publication: Intelligent Decision Technologies 2017
publication_types: 
- "1"
selected: false
summary: ""
title: Visual world paradigm data - From preprocessing to nonlinear time-course analysis
tags: 
- vwp
- eye-tracking
- gamm
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
