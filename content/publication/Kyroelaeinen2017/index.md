---
abstract: "In this study, we focus on the processing cost associated with morphologically complex verbs and explore how the distributional properties of morphological constructions influence real time processing. Specifically, we investigate Russian reflexive verbs (-sâ) which represent a salient category associated with changes in argument structures in Russian. To the best of our knowledge, this is the first study to explore the influence of morphological verb constructions during online processing of Russian reflexive verbs. Native Russian participants performed lexical decision on 160 tetramorphemic reflexive verbs, while their reaction times were recorded. In addition, each participant provided a semantic similarity estimation between the reflexive and the base verb on a five-point scale after completing the lexical decision task. For the reflexive verbs, distributional information covering nine frequency- and dispersion-based counts were extracted from the Russian National Corpus. These measures were subjected to principal component analysis to reduce the dimensionality of the data. This analysis showed that three components captured the morphological structuring, displaying a component for the whole morphological structure and two relative effects: one between the base and reflexive verb and another between the root and reflexive verb. The reaction times were analyzed with generalized additive mixed-effects modeling and the results showed that the three components significantly modulated reaction times in addition to perceived semantic similarity. The results showed that a matching operation between a given reflexive verb and a corresponding morphological construction facilitated processing whereas a competition inhibited. This supports the view according to which the processing cost of a given verb is related to its morphological distributions. These results, taken together, strongly support the notion of symbolic morphological constructions and that the processing cost of a morphologically complex verb is modulated by distributional properties and semantic similarity."
authors: 
- Aki-Juhani Kyröläinen
- Vincent Porretta
- Juhani Järvikivi
date: "2017-01-01"
featured: false
projects: ""
publication: Empirical approaches to cognitive linguistics - Analysing real-life data
publication_types: 
- "6"
selected: false
summary: ""
title: The role of morphological verb constructions in processing Russian reflexive verbs
tags: 
- reaction times
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
