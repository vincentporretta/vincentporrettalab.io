---
abstract: "This article examines the influence of gradient foreign accentedness on lexical competition during spoken word recognition. Using native and Mandarin-accented English words ranging in degree of foreign accentedness, we investigate the effect of increased accentedness on: a) the size of the competitor space, and b) the strength and duration of competitor activation. Here, we analyze the number of misperceptions in a transcription task, as well as the time course of competitor activation in a Visual World Paradigm eye-tracking task. The transcription data show that as accentedness increases, the number of unique misperceptions increases. This indicates that greater accent strength induces the activation of many additional competitors within the competition space relative to native speech. The eye-tracking data further show that, as accentedness increases, looks to competitors (not produced in the transcription task) increase both in likelihood and duration. This indicates that greater accentedness boosts the strength of competitor activation as well as the duration of the competition process, even when comprehension is ultimately successful, suggesting strong and diffuse competition within the lexicon. The results provide evidence of changes in the underlying dynamics which lead to the pervasive processing costs associated with foreign-accented speech that are commonly observed in behavioural data."
authors: 
- Vincent Porretta
- Aki-Juhani Kyröläinen
date: "2019-01-01"
doi: "https://dx.doi.org/10.1037/xlm0000674"
featured: false
projects: ""
publication: Journal of Experimental Psychology - Learning, Memory, and Cognition
publication_types: 
- "2"
selected: false
summary: ""
title: Influencing the time and space of lexical competition - The effect of gradient foreign accentedness
tags: 
- accented speech
- vwp
- eye-tracking
- gamm
- lexical competition
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
