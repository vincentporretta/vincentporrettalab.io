---
abstract: "Listeners perceive foreign-accented speech as different from native speech because it deviates from native speaker acoustic targets. These deviations may occur across many acoustic dimensions such as word duration, vowel duration, vowel formant values, and voice onset time. Accentedness ratings were modeled using ordinary least squares linear regression performed in R using the rms package. Prior to analysis, formant values were log transformed and plotted revealing considerable variation in vowel space. The vowel-to-word ratio was calculated by dividing the vowel duration of a word by the total duration of that word. If accentedness is a result of non-native productions approaching to varying degrees native-like acoustic targets, quantifying the distance from the native speaker norm allows examination of how variation along different variables affects perceived accentedness."
authors: 
- Vincent Porretta
- Benjamin V. Tucker
date: "2012-01-01"
featured: false
projects: ""
publication: Canadian Acoustics
publication_types: 
- "2"
selected: false
summary: ""
title: Predicting accentedness - Acoustic measurements of Chinese-accented English
tags: 
- accented speech
- perception
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
