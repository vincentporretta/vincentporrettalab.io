---
abstract: "In this study, we examined speaker-dependent (acoustic) and speaker-independent (lexical) linguistic influences on perceived foreign accentedness. Accentedness ratings assigned to Chinese-accented English words were analyzed, taking accentedness as a continuum. The speaker-dependent variables were included as acoustic distances, measured in relation to typical native-speaker values. The speaker-independent variable measures were related to the properties of individual words, not influenced by the speech signal. To the best of the authors’ knowledge, this represents the first attempt to examine speaker-dependent and speaker-independent variables simultaneously. The model indicated that the perception of accentedness is affected by both acoustic goodness of fit and lexical properties. The results are discussed in terms of matching variability in the input to multidimensional representations."
authors: 
- Vincent Porretta
- Aki-Juhani Kyröläinen
- Benjamin V. Tucker
date: "2015-01-01"
doi: "https://dx.doi.org/10.3758/s13414-015-0916-3"
featured: false
projects: ""
publication: Attention, Perception, & Psychophysics
publication_types: 
- "2"
selected: false
summary: ""
title: Perceived foreign accentedness - Acoustic distances and lexical properties
tags: 
- accented speech
- gamm
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
