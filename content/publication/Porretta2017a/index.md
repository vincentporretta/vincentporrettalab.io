---
abstract: "This study examines processing of native- and Chinese-accented English words. Event-related potentials (ERPs) were recorded for a set of spoken English words varying in degree of foreign accentedness. Waveform differences emerge for the Phonological Mapping Negativity (PMN) component across the continuum of foreign accentedness. Moreover, these differences in the response also vary across a measure of listener experience related to the amount of reported interaction listeners have with Chinese-accented speakers. The results are discussed in relation to online processing of speech variability, long-term exposure to the variability inherent to foreign-accented speech, and the functional significance of the PMN."
authors: 
- Vincent Porretta
- Antoine Tremblay
- Patrick Bolger
date: "2017-01-01"
doi: "https://dx.doi.org/10.1016/j.jneuroling.2017.03.002"
featured: false
projects: ""
publication: Journal of Neurolinguistics
publication_types: 
- "2"
selected: false
summary: ""
title: Got experience? PMN amplitudes to foreign-accented speech modulated by listener experience
tags: 
- accented speech
- erp
- gamm
- listener experience
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
