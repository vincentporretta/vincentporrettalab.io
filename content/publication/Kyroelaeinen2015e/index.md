---
abstract: "NA"
authors: 
- Aki-Juhani Kyröläinen
- Vincent Porretta
- Juhani Järvikivi
date: "2015-01-01"
featured: false
projects: ""
publication: Journal of Eye Movement Research - Abstracts of the 18th European Conference on Eye Movements
publication_types: 
- "1"
selected: false
summary: ""
title: The role of morpho-semantic information in processing Russian verbal aspect - Evidence from pupil dilation, fixation durations and reaction times
tags: 
- pupil dilation
- fixation durations
- reaction times
- eye-tracking
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
