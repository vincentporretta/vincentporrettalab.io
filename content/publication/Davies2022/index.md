---
abstract: "Addressees use information from specific speakers’ previous discourse to make predictions about incoming linguistic material and to restrict the choice of potential interpretations. In this way, speaker specificity has been shown to be an influential factor in language processing across several domains e.g., spoken word recognition, sentence processing, and pragmatics. However, its influence on semantic disambiguation has received little attention to date. Using an exposure-test design and visual world eye tracking, we examined the effect of speaker-specific literal vs. nonliteral style on the disambiguation of metaphorical polysemes such as ‘fork’, ‘head’, and ‘mouse’. Eye movement data revealed that when interpreting polysemous words with a literal and a nonliteral meaning, addressees showed a late-stage preference for the literal meaning in response to a nonliteral speaker. We interpret this as reflecting an indeterminacy in the intended meaning in this condition, as well as the influence of meaning dominance cues at later stages of processing. Response data revealed that addressees then ultimately resolved to the literal target in 90% of trials. These results suggest that addressees consider a range of senses in the earlier stages of processing, and that speaker style is a contextual determinant in semantic processing."
authors: 
- Catherine Davies
- Vincent Porretta
- Kremena Koleva
- Ekaterini Klepousniotou
date: "2022-01-01"
doi: "https://dx.doi.org/10.1007/s10936-022-09852-0"
featured: false
projects: ""
publication: Journal of Psycholinguistic Research
publication_types: 
- "2"
selected: false
summary: ""
title: Speaker-specific cues influence semantic disambiguation
tags: 
- vwp
- eye-tracking
- semantics
- language processing
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
