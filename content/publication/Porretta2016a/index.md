---
abstract: "The present article examines lexical processing of foreign-accented speech, specifically as it relates to gradient foreign accentedness and listener experience. In two experiments, we investigate the effect of accentedness and experience on the strength of lexical activation and the time-course of word recognition utilizing native- and Mandarin-accented English words. Gradient and non-linear patterns emerged for both accentedness and experience. Experiment 1 employed cross-modal identity priming and the analysis of reaction times indicates that tokens with a greater degree of accentedness result in a reduced effectiveness of the identity prime. Listener experience with Chinese-accented English positively influenced activation strength in a gradient fashion. Experiment 2 employed visual world eye-tracking and the analysis of looks to the target word indicates that the time-course of recognition differs across the accentedness continuum, slowing as accentedness increases. Again, listener experience improved the time-course of word recognition. The results are discussed in terms of foreign-accented speech processing and long-term adaptation to non-native variability and suggest the need for a dynamic systems approach."
authors: 
- Vincent Porretta
- Benjamin V. Tucker
- Juhani Järvikivi
date: "2016-01-01"
doi: "https://dx.doi.org/10.1016/j.wocn.2016.05.006"
featured: false
projects: ""
publication: Journal of Phonetics
publication_types: 
- "2"
selected: false
summary: ""
title: The influence of gradient foreign accentedness and listener experience on word recognition
tags: 
- accented speech
- reaction times
- vwp
- eye-tracking
- gamm
- listener experience
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
