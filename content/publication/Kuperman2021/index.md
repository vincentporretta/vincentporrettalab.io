---
abstract: "Is it possible that silent reading rate is the same as the most efficient listening rate? The hypothesis has been formulated in the past, but never got much traction because silent reading is almost twice as fast as typical speech. On the other hand, several studies have shown that listening comprehension retains high quality for spoken materials presented at speeds up to 275 words per minute (wpm), and a recent meta-analysis has also shown that reading rate is lower than often thought: 240-260 wpm on average. To address the question above, we ran a new study specifically comparing spontaneous silent reading rate with comprehension of speech presented at different rates within the same participants and using matched texts. We replicated the finding that listening comprehension was not hindered at the speech rate of 270 wpm but showed a steep decline at the rate of 315 wpm. Thus, the most efficient observed listening rate was on par with the spontaneous reading rate for the same texts (269 wpm on average). Therefore, we conclude that listening and reading follow the same time constraints."
authors: 
- Victor Kuperman
- Aki-Juhani Kyröläinen
- Vincent Porretta
- Marc Brysbaert
- Sophia Yang
date: "2021-01-01"
doi: "https://dx.doi.org/10.1037/xhp0000932"
featured: false
projects: ""
publication: Journal of Experimental Psychology - Human Perception and Performance
publication_types: 
- "2"
selected: false
summary: ""
title: A lingering question addressed - Reading rate and most efficient listening rate are highly similar
tags: ""
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
