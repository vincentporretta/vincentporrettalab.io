---
abstract: "Filled pause disfluencies (e.g., uh) elicit a disfluency bias in listeners: listeners predict that a speaker will name an unfamiliar object, versus a familiar one, when both are equally plausible referents. When listeners receive speaker-specific information indicating that disfluency is not reliably tied to word familiarity, the disfluency bias can be suspended. The first aim of this study was to determine if stuttered disfluencies would also elicit the disfluency bias in listeners. The second aim of this study was to determine if informing the listener that they will hear a speaker who stutters, would suspend the disfluency bias. Eye tracking data from 52 participants were analyzed using a 2 (acknowledgement) x 2 (target type) x 3 (fluency) mixed ANOVA. The dependent variable was the proportion of looks to the competitor object. The disfluency bias was found with typical and stuttered disfluencies when the target type was unfamiliar. Acknowledgement of stuttering did not suspend the bias."
authors: 
- Catherine Leonard
- Juhani Järvikivi
- Vincent Porretta
- Marilyn Langevin
date: "2016-01-01"
doi: "https://dx.doi.org/10.21437/SpeechProsody.2016-250"
featured: false
projects: ""
publication: Speech Prosody 2016
publication_types: 
- "1"
selected: false
summary: ""
title: Processing of stuttered speech by fluent listeners
tags: 
- disfluency
- vwp
- eye-tracking
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
