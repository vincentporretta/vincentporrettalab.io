---
abstract: "This study investigated the time course of activation of orthographic information in spoken word recognition with two visual world eye-tracking experiments in a task where second language (L2) spoken word forms had to be matched with their printed referents. Participants (n = 64) were native Finnish learners of L2 French ranging from beginners to highly proficient. In Experiment 1, L2 targets (e.g., <cidre> /sidʀ/) were presented with either orthographically overlapping onset competitors (e.g., <cintre> /sɛ̃tʀ/) or phonologically overlapping onset competitors (<cycle> /sikl/). In Experiment 2, L2 targets (e.g., <paume> /pom/) were associated with competitors in Finnish, L1 of the participants, in conditions symmetric to Experiment 1 (<pauhu> /pauhu/ vs. <pommi> /pom:i/). In the within-language experiment (Experiment 1), the difference in target identification between the experimental conditions was not significant. In the between-language experiment (Experiment 2), orthographic information impacted the mapping more in lower proficiency learners, and this effect was observed 600 ms after the target word onset. The influence of proficiency on the matching was nonlinearproficiency impacted the mapping significantly more in the lower half of the proficiency scale in both experiments. These results are discussed in terms of coactivation of orthographic and phonological information in L2 spoken word recognition."
authors: 
- Outi Veivo
- Vincent Porretta
- Juhani Järvikivi
- Jukka Hyönä
date: "2018-01-01"
doi: "https://dx.doi.org/10.1017/S0142716418000103"
featured: false
projects: ""
publication: Applied Psycholinguistics
publication_types: 
- "2"
selected: false
summary: ""
title: Spoken L2 words activate L1 orthographic information in late L2 learners
tags: 
- second language learning
- vwp
- eye-tracking
- gamm
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
