---
abstract: "Native-speaker listeners constantly predict upcoming units of speech as part of language processing, using various cues. However, this process is impeded in second-language listeners, as well as when the speaker has an unfamiliar accent. Whereas previous research has largely concentrated on the pronunciation of individual segments in foreign-accented speech, we show that regional accent impedes higher levels of language processing, making native listeners’ processing resemble that of second-language listeners. In Experiment 1, 42 native speakers of Canadian English followed instructions spoken in British English to move objects on a screen while their eye movements were tracked. Native listeners use prosodic cues to information status to disambiguate between two possible referents, a new and a previously mentioned one, before they have heard the complete word. By contrast, the Canadian participants, similarly to second-language speakers, were not able to make full use of prosodic cues in the way native British listeners do. In Experiment 2, 19 native speakers of Canadian English rated the British English instructions used in Experiment 1, as well as the same instructions spoken by a Canadian imitating the British English prosody. While information status had no effect for the Canadian imitations, the original stimuli received higher ratings when prosodic realization and information status of the referent matched than for mismatches, suggesting a native-like competence in these offline ratings. These findings underline the importance of expanding psycholinguistic models of second language/dialect processing and representation to include both prosody and regional variation."
authors: 
- Anja Arnhold
- Vincent Porretta
- Aoju Chen
- Saskia A. J. M. Verstegen
- Ivy Mok
- Juhani Järvikivi
date: "2020-01-01"
doi: "https://dx.doi.org/10.3758/s13423-020-01731-w"
featured: false
projects: ""
publication: Psychonomic Bulletin & Review
publication_types: 
- "2"
selected: false
summary: ""
title: (Mis)understanding your native language - Regional accent impedes processing of information status
tags: 
- accent
- information status
- vwp
- eye-tracking
- prediction
- gamm
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
