---
abstract: "Listeners use linguistic information and real-world knowledge to predict upcoming spoken words. However, studies of predictive processing have focused on prediction under optimal listening conditions. We examine the effect of foreign-accented speech on predictive processing. Further, we investigate whether accent-specific experience facilitates predictive processing. Using the visual world paradigm, we demonstrate that while the presence of an accent impedes predictive processing, it does not preclude it. We further show that as listener experience increases, predictive processing for accented speech increases and begins to approximate the pattern seen for native speech. The results speak to the limitation of processing resources which must be allocated, leading to a trade-off when faced with increased uncertainty and more effortful recognition due to a foreign accent."
authors: 
- Vincent Porretta
- Lori Buchanan
- Juhani Järvikivi
date: "2020-01-01"
doi: "https://dx.doi.org/10.3758/s13414-019-01946-7"
featured: false
projects: ""
publication: Attention, Perception, & Psychophysics
publication_types: 
- "2"
selected: false
summary: ""
title: When processing costs impact predictive processing - The case of foreign-accented speech and accent experience
tags: 
- accented speech
- vwp
- eye-tracking
- prediction
- gamm
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
