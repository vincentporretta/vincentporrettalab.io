---
abstract: "Intelligibility and degree of accentedness are interrelated aspects of non-native speech. Previous research suggests that foreign accentedness is influenced by phonetic distance measures. These distance measures may also influence the intelligibility of individual words. In the present study we further investigate the relationship between the intelligibility of native- and Mandarin-accented English words and acoustic distance measures (both spectral and temporal). We also examine the functional relationship between intelligibility and ratings of foreign accentedness assigned to the same words. Intelligibility was based on transcription accuracy scores and acoustic distances were based on formant and duration measurements in relation to mean values from a set of native talkers. The results indicate that temporal and spectral distances influence word intelligibility and that the functional relationship between intelligibility and accentedness is non-linear."
authors: 
- Vincent Porretta
- Benjamin V. Tucker
date: "2015-01-01"
featured: false
projects: ""
publication: Proceedings of the 18th International Congress of Phonetic Sciences
publication_types: 
- "1"
selected: false
summary: ""
title: Intelligibility of foreign-accented words - Acoustic distances and gradient foreign accentedness
tags: 
- non-refereed
- accented speech
- gamm
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
