---
abstract: "The present investigation examines English speakers’ ability to identify and discriminate non-native consonant length contrast. Three groups (L1 English No-Instruction, L1 English Instruction, and L1 Finnish control) performed a speeded forced-choice identification task and a speeded AX discrimination task on Finnish non-words (e.g. /hupo/-/huppo/) which were manipulated for intervocalic consonant duration. The results indicate that basic information, focusing the participants’ attention on a particular contrast, assists novice listeners in processing a non-native contrast. We find support for a phonetic level of processing which is intermediate to non-linguistic acoustic processing and phonemic processing at which the phonetic cue of duration becomes significant. We interpret the results in relation to the Speech Learning Model (Flege 1995, 2003)."
authors: 
- Vincent Porretta
- Benjamin V. Tucker
date: "2015-01-01"
doi: "https://dx.doi.org/10.1177/0267658314559573"
featured: false
projects: ""
publication: Second Language Research
publication_types: 
- "2"
selected: false
summary: ""
title: Perception of non-native consonant length contrast - The role of attention in phonetic processing.
tags: 
- second language learning
- perception
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
