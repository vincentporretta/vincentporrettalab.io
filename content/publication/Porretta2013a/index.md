---
abstract: "English speakers are sensitive to phonetic length (Pickett & Decker, 1960) but do not maintain a phonemic length contrast (Hayes, 2002). This study examines English speakers' ability to discriminate and identify intervocalic consonant length in Finnish non-words. The consonants were manipulated for length and half of the participants were given brief written instruction regarding the Finnish length contrast. In an AX discrimination task, participants responded to increasing contrast ratio gradiently; however, the instructed group performed significantly better than the uninstructed group. Proficiency in any second language aids contrast detection in those receiving no instruction. In a forced-choice identification task, participants showed no evidence of boundary effects; however, the instructed group performed significantly more like native Finnish controls. Again, second language proficiency aids in consonant length detection. The present results indicate that information about and attention to a novel contrast, along with second language experience, aid processing in novice listeners. Given that learners can eventually maintain native-like contrasts, these factors may be influential in the initial formation of L2 phonological representations and support phonetic level of processing (Werker & Tees, 1984), intermediate to non-linguistic acoustic processing and phonemic processing, at which acoustic duration becomes a phonetically relevant cue."
authors: 
- Vincent Porretta
- Benjamin V. Tucker
date: "2013-01-01"
doi: "https://dx.doi.org/10.1121/1.4800677"
featured: false
projects: ""
publication: ICA 2013 Montreal
publication_types: 
- "1"
selected: false
summary: ""
title: Perception of non-native consonant length in naïve English listeners
tags: 
- non-refereed
- second language learning
- perception
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
