---
abstract: "This study examines listening effort, as indexed by pupil dilation, needed for processing foreign-accented speech that varies in intelligibility. Previous research has shown that the magnitude of pupil dilation is influenced by various factors, crucially the amount of noise added to speech. However, the method has not yet been used to examine foreign-accented speech. Here, we determine if the full range of foreign accent intelligibility induces a similar increase in cognitive processing effort as that seen for speech in noise. Further, we examine whether listener experience with the accent mitigates this increase in cognitive effort. The results indicate that as speech becomes less intelligible due to accent, pupil dilation increases. Additionally, experience not only reduces the overall magnitude of pupil dilation, it shifts the threshold at which decreased intelligibility begins to incur additional processing effort. We discuss the present results in terms of listening effort when processing spoken variability. The present study establishes pupillometry as an informative method for investigating the processing demands associated with foreign-accented speech."
authors: 
- Vincent Porretta
- Benjamin V. Tucker
date: "2019-01-01"
doi: "https://dx.doi.org/10.3389/fcomm.2019.00008"
featured: false
projects: ""
publication: Frontiers in Communication
publication_types: 
- "2"
selected: false
summary: ""
title: Eyes wide open - Pupillary response to a foreign accent varying in intelligibility
tags: 
- accented speech
- pupil dilation
- eye-tracking
- gamm
- listener experience
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
