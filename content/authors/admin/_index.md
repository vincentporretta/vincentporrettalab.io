---
authors:
- admin
bio: 
education:
  courses:
  - course: PhD in Linguistics
    institution: University of Alberta
    year: 2015
  - course: MEd in Foreign and Second Language Teaching
    institution: Ohio State University
    year: 2007
  - course: MA in Italian
    institution: Ohio State University
    year: 2006
  - course: BA in Italian
    institution: Ohio State University
    year: 2004
email: ""
interests:
- Psycholinguistics
- Spoken language processing
- Foreign-accented speech
- Visual World Paradigm eye-tracking
- Second language acquisition and Bilingualism
organizations:
- name: Amazon
  url: ""
role: Language Engineer
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'
- icon: researchgate
  icon_pack: ai
  link: https://www.researchgate.net/profile/Vincent_Porretta
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=YfzZRLIAAAAJ&hl=en
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/vincent-porretta-linguist
superuser: true
title: Vincent Porretta
user_groups:
- Researchers
- Visitors
---

As a language engineer at Amazon, I develop and maintain the language models underlying Alexa that capture the meaning a customer intends when interacting with Alexa.
&nbsp;&nbsp;